#!/bin/bash

# nginx Startup script for the Nginx HTTP Server
# it is v.0.0.2 version.
# chkconfig: - 85 15
# description: Nginx is a high-performance web and proxy server.
#              It has a lot of features, but it's not for everyone.
# processname: nginx

nginxd=/data/proxy_pool/src/sbin/nginx

nginx_config=/data/proxy_pool/src/conf/nginx.conf

nginx_pid=/data/proxy_pool/logs/nginx.pid

nginx_lock=/data/proxy_pool/logs/nginx.lock

RETVAL=0    

prog="nginx"    

# Source function library.    

.  /etc/rc.d/init.d/functions    

# Source networking configuration.    

.  /etc/sysconfig/network    

# Check that networking is up.    

[ "$NETWORKING" = "no" ] && exit 0    

[ -x $nginxd ] || exit 0    

# Start nginx daemons functions.    

start() {    

if [ -e $nginx_pid ];then    

  echo "nginx already running...."    

  exit 1    

fi    

  echo -n $"Starting $prog: "    

  daemon $nginxd -c ${nginx_config}    

  RETVAL=$?    

  echo    

  [ $RETVAL = 0 ] && touch ${nginx_lock}

  return $RETVAL    

}    

# Stop nginx daemons functions.    

stop() {    

       echo -n $"Stopping $prog: "    

       killproc $nginxd    

       RETVAL=$?    

       echo    

       [ $RETVAL = 0 ] && rm -f ${nginx_pid} ${nginx_lock}

}    

reload() {    

   echo -n $"Reloading $prog: "    

   #kill -HUP `cat ${nginx_pid}`    

   killproc $nginxd -HUP    

   RETVAL=$?    

   echo    

}    

# See how we were called.    

case "$1" in    

start)    

       start    

       ;;    

stop)    

       stop    

       ;;    

reload)    

       reload    

       ;;    

restart)    

       stop    

       start    

       ;;    

               

status)    

       status $prog    

       RETVAL=$?    

       ;;    

*)    

       echo $"Usage: $prog {start|stop|restart|reload|status|help}"    

       exit 1    

esac    

exit $RETVAL

